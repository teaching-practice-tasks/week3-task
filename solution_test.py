import unittest

from solution import flatten_nested_list


class TestSolution(unittest.TestCase):
    def test_1(self):
        assert flatten_nested_list([1, [2, 3], [4, [5, 6], 7]]) == [1, 2, 3, 4, 5, 6, 7]

    def test_2(self):
        assert flatten_nested_list([[1, 2], 3, [4, [5, 6], 7]]) == [1, 2, 3, 4, 5, 6, 7]

    def test_3(self):
        assert flatten_nested_list([1, [[2, 3], 4], [5, 6, 7]]) == [1, 2, 3, 4, 5, 6, 7]

    def test_4(self):
        assert flatten_nested_list([[1, 2, 3], [4, [5, 6, 7]]]) == [1, 2, 3, 4, 5, 6, 7]

    def test_5(self):
        assert flatten_nested_list([[1, 2, 3], [4, [5, 6], 7], [[8, 9], 10]]) == [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
