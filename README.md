### Week 3: Flatten Nested List

Write a Python function called `flatten_nested_list(lst)` that takes in a list of integers and returns the list with all the nested lists flattened into a single list of integers. **The function should use recursion to solve the problem**.

For example, `flatten_nested_list([1, [2, 3], [4, [5, 6], 7]])` should return `[1, 2, 3, 4, 5, 6, 7]` because it flattens the nested lists into a single list of integers.

Examples:
```
flatten_nested_list([1, [2, 3], [4, [5, 6], 7]]) => [1, 2, 3, 4, 5, 6, 7]
flatten_nested_list([[1, 2], 3, [4, [5, 6], 7]]) => [1, 2, 3, 4, 5, 6, 7]
```

You have to implement function `flatten_nested_list(lst)` in `solution.py` file.
You may create additional functions.